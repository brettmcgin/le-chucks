package com.brettmcgin.lechucks

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import com.brettmcgin.lechucks.mvi.ViewState
import com.brettmcgin.lechucks.site.Site
import com.brettmcgin.lechucks.site.Site.Greenwood
import com.brettmcgin.lechucks.site.SitePicker
import com.brettmcgin.lechucks.tap.TapApi
import com.brettmcgin.lechucks.tap.TapRepository
import com.brettmcgin.lechucks.tap.TapTable
import com.brettmcgin.lechucks.tap.TapViewModel
import com.brettmcgin.lechucks.ui.theme.LeChucksTheme

class MainActivity : ComponentActivity() {
    private val tapApi by lazy { TapApi(ktorHttpClient) }
    private val tapRepository by lazy { TapRepository(tapApi) }
    private val tapViewModel by lazy { TapViewModel(tapRepository) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            LeChucksTheme {
                App(tapViewModel)
            }
        }
    }
}

@Composable
private fun App(tapViewModel: TapViewModel) = Surface(modifier = Modifier.fillMaxSize()) {
    var currentSite: Site by rememberSaveable { mutableStateOf(Greenwood) }
    val state by tapViewModel.state.collectAsState()
    val taps = state.data?.taps.orEmpty()
        .filterNot { tap -> with(tap.name) { startsWith("_") || startsWith("-") } }

    if (state is ViewState.Empty) tapViewModel.load(currentSite)

    Column {
        SitePicker() { currentSite = it.also(tapViewModel::load) }
        TapTable(taps)
    }
}
