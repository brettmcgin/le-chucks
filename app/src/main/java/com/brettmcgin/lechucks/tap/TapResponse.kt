package com.brettmcgin.lechucks.tap

import kotlinx.serialization.Serializable

@Serializable
@Suppress("ConstructorParameterNaming")
data class TapResponse(
    val `$Override`: String? = null,
    val `Keg$`: String? = null,
    val NoGr: String? = null,
    val Size: String? = null,
    val Special: String? = null,
    val abv: String? = null,
    val beer: String? = null,
    val color: String? = null,
    val costOz: String? = null,
    val crowler: String? = null,
    val growler: String? = null,
    val origin: String? = null,
    val oz: String? = null,
    val price: String? = null,
    val priceOz: String? = null,
    val serving: String? = null,
    val shop: String? = null,
    val tap: String? = null,
    val type: String? = null
)
