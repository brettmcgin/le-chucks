package com.brettmcgin.lechucks.tap

import com.brettmcgin.lechucks.site.Site
import io.ktor.client.HttpClient
import io.ktor.client.request.get

private const val TAP_LIST_BASE_URL = "https://taplists.web.app/data?menu="

class TapApi(private val client: HttpClient) {
    suspend fun getTaps(site: Site): List<TapResponse> =
        client.get("$TAP_LIST_BASE_URL${site.siteKey}")
}
