package com.brettmcgin.lechucks.tap

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color.Companion.DarkGray
import androidx.compose.ui.graphics.Color.Companion.LightGray

@Composable
fun TapTable(taps: List<Tap>) =
    LazyColumn { taps.forEachIndexed { index, tap -> item { tapRow(tap, index % 2 == 0) } } }

@Composable
private fun tapRow(tap: Tap, isDark: Boolean = true) =
    Row(
        Modifier
            .fillMaxWidth()
            .background(DarkGray.takeIf { isDark } ?: LightGray),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Text(tap.position.toString())
        Text(tap.name)
        Text(
            when {
                tap.serving.contains("8") -> "S"
                tap.serving.contains("20") -> "L"
                else -> "M"
            }
        )
    }
