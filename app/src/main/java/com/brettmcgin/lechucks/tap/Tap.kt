package com.brettmcgin.lechucks.tap

data class Tap(
    val position: Int,
    val name: String,
    val special: String = "",
    val types: List<String> = emptyList(),
    val crowlerPrice: Double = 0.0,
    val growlerPrice: Double = 0.0,
    val abv: Double = 0.0,
    val serving: String = ""
) {
    val isCrowlerable = crowlerPrice > 0.0
    val isGrowlerable = growlerPrice > 0.0
}
