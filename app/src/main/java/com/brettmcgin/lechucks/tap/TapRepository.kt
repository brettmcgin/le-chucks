package com.brettmcgin.lechucks.tap

import com.brettmcgin.lechucks.site.Site

private fun TapResponse.toTap(): Tap = Tap(
    position = tap?.toInt() ?: 0,
    name = beer.orEmpty(),
    special = Special.orEmpty(),
    types = type?.split(", ").orEmpty(),
    crowlerPrice = crowler?.toDouble() ?: 0.0,
    growlerPrice = growler?.toDouble() ?: 0.0,
    abv = abv?.toDouble() ?: 0.0,
    serving = serving.orEmpty()
)

class TapRepository(
    private val tapApi: TapApi
) {
    suspend fun getTaps(site: Site): List<Tap> = tapApi.getTaps(site).map(TapResponse::toTap)
}
