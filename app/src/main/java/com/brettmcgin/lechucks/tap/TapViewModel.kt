package com.brettmcgin.lechucks.tap

import com.brettmcgin.lechucks.mvi.StateViewModel
import com.brettmcgin.lechucks.mvi.ViewState
import com.brettmcgin.lechucks.site.Site
import com.brettmcgin.lechucks.tap.TapViewModel.Intent
import com.brettmcgin.lechucks.tap.TapViewModel.Intent.Load
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

data class TapsViewState(
    val taps: List<Tap> = listOf(),
    val site: Site
)

class TapViewModel(private val tapRepository: TapRepository) :
    StateViewModel<Intent, TapsViewState>() {
    sealed class Intent {
        data class Load(val site: Site) : Intent()
    }

    override fun transformIntents(intentFlow: Flow<Intent>): Flow<ViewState<TapsViewState>> =
        intentFlow.map {
            when (it) {
                is Load -> TapsViewState(tapRepository.getTaps(it.site), it.site)
            }
        }.toViewState()

    fun load(site: Site = Site.Greenwood) = emitIntent(Load(site))
}
