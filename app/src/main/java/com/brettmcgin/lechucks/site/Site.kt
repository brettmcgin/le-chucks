package com.brettmcgin.lechucks.site

import android.os.Parcelable
import androidx.compose.runtime.Immutable
import kotlinx.parcelize.Parcelize

typealias SiteKey = String

@Immutable
sealed class Site(val siteKey: SiteKey, val name: String) : Parcelable {
    @Parcelize object Greenwood : Site("GW", "Greenwood")
    @Parcelize object SewardPark : Site("SP", "Seward Park")
    @Parcelize object CentralDistrict : Site("CD", "Central District")
}
