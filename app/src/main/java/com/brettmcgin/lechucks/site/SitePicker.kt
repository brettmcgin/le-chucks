package com.brettmcgin.lechucks.site

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme.typography
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import com.brettmcgin.lechucks.R
import com.brettmcgin.lechucks.site.Site.CentralDistrict
import com.brettmcgin.lechucks.site.Site.Greenwood
import com.brettmcgin.lechucks.site.Site.SewardPark

private val locations = listOf(Greenwood, SewardPark, CentralDistrict)

@Composable
fun SitePicker(site: Site = Greenwood, onSiteSelected: (Site) -> Unit) {
    var expanded by remember { mutableStateOf(false) }
    var currentSite by rememberSaveable { mutableStateOf(site) }

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentSize(Alignment.TopStart)
    ) {
        CurrentSite(
            site = currentSite,
            onClick = { expanded = true }
        )

        SiteDropDownMenu(
            isExpanded = expanded,
            onDismissRequest = { expanded = false },
            onDropDownMenuItemClick = {
                expanded = false
                currentSite = it
                onSiteSelected(currentSite)
            }
        )
    }
}

@Composable
private fun CurrentSite(site: Site, onClick: () -> Unit) = ConstraintLayout(
    modifier = Modifier.fillMaxWidth(),
) {
    val (siteTitle, icon) = createRefs()

    Text(
        site.name,
        modifier = Modifier
            .clickable(onClick = { onClick() })
            .constrainAs(siteTitle) {
                end.linkTo(parent.end)
                start.linkTo(parent.start)
                top.linkTo(parent.top)
                bottom.linkTo(parent.bottom)
            },
        fontSize = typography.h4.fontSize,
        fontStyle = typography.h4.fontStyle
    )

    Icon(
        modifier = Modifier
            .size(32.dp)
            .constrainAs(icon) {
                end.linkTo(parent.end)
                top.linkTo(parent.top)
                bottom.linkTo(parent.bottom)
            },
        painter = painterResource(id = R.drawable.ic_arrow_drop_down),
        contentDescription = "Location Picker",
        tint = typography.h4.color,
    )
}

@Composable
private fun SiteDropDownMenu(
    isExpanded: Boolean,
    onDismissRequest: () -> Unit,
    onDropDownMenuItemClick: (Site) -> Unit
) = DropdownMenu(
    expanded = isExpanded,
    onDismissRequest = { onDismissRequest() },
    modifier = Modifier.fillMaxWidth()
) {
    locations.forEachIndexed { index, _ ->
        DropdownMenuItem(onClick = { onDropDownMenuItemClick(locations[index]) }) {
            Text(text = locations[index].name)
        }
    }
}
